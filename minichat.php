<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="refresh" content="20" />
        <title>Mini-chat</title>
    </head>
    <style>
        form
        {
            text-align: center;
        }
    </style>
    <body>

<form method="post" action="minichat_post.php">
    <p>
        <label for="pseudo">Pseudo</label> : <input type="text" name="pseudo" value=<?php if(isset($_COOKIE['pseudo'])) { echo $_COOKIE['pseudo']; } else {?> "Taper un pseudo" <?php } ?> /><br />
        
        <label for="pseudo">Message</label> : <input type="text" name="message" /><br />
        
        <input type="submit" name="Valider" />
    </p>
</form>

<?php

try
{
    $bdd = new PDO('mysql:host=localhost;dbname=test;charset=utf8', 'root', '');
}
catch (Exception $e)
{
    die('Erreur : ' . $e->getMessage());
}

$messagesParPage=10; //Nous allons afficher 10 messages par page.

$retour_total=$bdd->query('SELECT COUNT(*) AS total FROM minichat'); //Nous récupérons le contenu de la requête dans $retour_total
$donnees_total=$retour_total->fetch(); //On range retour sous la forme d'un tableau.
$total=$donnees_total['total']; //On récupère le total pour le placer dans la variable $total.
 
//Nous allons maintenant compter le nombre de pages.
$nombreDePages=ceil($total/$messagesParPage);
 
if(isset($_GET['page'])) // Si la variable $_GET['page'] existe...
{
     $pageActuelle=intval($_GET['page']);
 
     if($pageActuelle>$nombreDePages) // Si la valeur de $pageActuelle (le numéro de la page) est plus grande que $nombreDePages...
     {
          $pageActuelle=$nombreDePages;
     }
}
else // Sinon
{
     $pageActuelle=1; // La page actuelle est la n°1    
}

$premiereEntree=($pageActuelle-1)*$messagesParPage; // On calcul la première entrée à lire

$retour_messages=$bdd->query('SELECT date_entree, pseudo, message FROM minichat ORDER BY ID DESC LIMIT '.$premiereEntree.', '.$messagesParPage.''); 

while ($donnees_messages=$retour_messages->fetch()) // On lit les entrées une à une grâce à une boucle
{
     //Je vais afficher les messages dans des petits tableaux. C'est à vous d'adapter pour votre design...
     //De plus j'ajoute aussi un nl2br pour prendre en compte les sauts à la ligne dans le message.
    echo '<p>' . htmlspecialchars($donnees_messages['date_entree']) . ' : <strong>' . htmlspecialchars($donnees_messages['pseudo']) . ' : </strong>' . htmlspecialchars($donnees_messages['message']) . '</p>';
}

$retour_messages->closeCursor();

echo '<p align="center"> Page : '; //Pour l'affichage, on centre la liste des pages

for($i=1; $i<=$nombreDePages; $i++) //On fait notre boucle
{
     //On va faire notre condition
     if($i==$pageActuelle) //S'il s'agit de la page actuelle...
     {
         echo ' [ '.$i.' ] '; 
     }    
     else //Sinon...
     {
          echo ' <a href="minichat.php?page='.$i.'">'.$i.'</a> ';
     }
}

echo '</p>';
?>
      
    </body>
</html>