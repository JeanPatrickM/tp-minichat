<?php

setcookie('pseudo', $_POST['pseudo'], time() + 365*24*3600, null, null, false, true);

try
{
	$bdd = new PDO('mysql:host=localhost;dbname=test;charset=utf8', 'root', '');
}
catch (Exception $e)
{
	die('Erreur : ' . $e->getMessage());
}

if(isset($_COOKIE['pseudo'])) 
{ 
    $req = $bdd->prepare('INSERT INTO minichat (date_entree, pseudo, message) VALUES(?, ?, ?)');
    $req->execute(array($_POST['date_entree'],$_POST['pseudo'], $_POST['message']));
}

header('Location: minichat.php');

?>